package com.example.dddsurvey.presentation

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import javax.validation.ConstraintViolation
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.openapi.example.v2.model.AwesomeType
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.Resource
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.OffsetDateTime
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory


@SpringBootTest
class DtoOverrideTest {

    private lateinit var validator: Validator

    @BeforeEach
    fun setup() {
        println("###")
        val factory: ValidatorFactory = Validation.buildDefaultValidatorFactory()
        this.validator = factory.validator
    }

    @Test
    fun canOverride() {
        val invalidObject = AwesomeType(
                "##",
                2,
                1,
                1.0.toFloat(),
                1.0,
                "#".toByteArray(),
                ByteArrayResource(ByteArray(5),"##"),
                LocalDate.now(),
                OffsetDateTime.now(),
                "###"
        )
        val constraintViolations: Set<ConstraintViolation<AwesomeType>> = validator.validate(invalidObject)
        assert(constraintViolations.isEmpty())
    }

    @Test
    fun cannotOverride() {
        val invalidObject = AwesomeType(
                "##",
                2,
                1,
                1.0.toFloat(),
                1.0,
                "#".toByteArray(),
                ByteArrayResource(ByteArray(5),"##"),
                LocalDate.now(),
                OffsetDateTime.now(),
                "###"
        )
        val constraintViolations: Set<ConstraintViolation<AwesomeType>> = validator.validate(invalidObject)
        assertThat(constraintViolations.size).isEqualTo(1)
    }
}