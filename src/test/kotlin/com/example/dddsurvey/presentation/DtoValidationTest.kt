package com.example.dddsurvey.presentation

import com.example.dddsurvey.presentation.dto.ValidationSampleCar
import com.example.dddsurvey.presentation.dto.ValidationSampleObject
import org.aspectj.lang.annotation.Before
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import javax.validation.ConstraintViolation
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import java.time.LocalDateTime
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory
import javax.validation.constraints.Max
import javax.validation.constraints.Pattern


@SpringBootTest
class DtoValidationTest {

    private lateinit var validator: Validator

    @BeforeEach
    fun setup() {
        println("###")
        val factory: ValidatorFactory = Validation.buildDefaultValidatorFactory()
        this.validator = factory.validator
    }

    /**
     * BeanのValidationのサンプル
     */

    /**
     * プロパティとして定義していれば当然バリデーションされる
     */
    @Test
    fun inputIsInValid() {
        class ValidationSampleInput {
            @Max(10)
            var numberBetweenOneAndTen = 0

            @Pattern(regexp = "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$")
            var ipAddress: String? = null
        }

        val input = ValidationSampleInput()
        input.ipAddress = "fewhgoei2g234"
        input.numberBetweenOneAndTen = 22
        val constraintViolations: Set<ConstraintViolation<ValidationSampleInput>> = validator.validate(input)
        assertThat(constraintViolations.size).isEqualTo(2)
    }


    /**
     * コンストラクタ変数として定義しているとバリデーションされない！！
     */
    @Test
    fun inputIsInValidButNoValidationError() {
        class ValidationSampleInput(@Max(10)
                                    var numberBetweenOneAndTen: Int,
                                    @Pattern(regexp = "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$")
                                    var ipAddress: String) {
        }

        val input = ValidationSampleInput(22, "fewhgoei2g234")
        val constraintViolations: Set<ConstraintViolation<ValidationSampleInput>> = validator.validate(input)
        assertThat(constraintViolations.size).isEqualTo(0)
    }

    /**
     * `@field:`を付与するとバリデーションされる
     */
    @Test
    fun inputIsInValidWithConstructParameter() {
        class ValidationSampleInput(@field:Max(10)
                                    var numberBetweenOneAndTen: Int,
                                    @field:Pattern(regexp = "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$")
                                    var ipAddress: String) {
        }

        val input = ValidationSampleInput(22, "fewhgoei2g234")
        val constraintViolations: Set<ConstraintViolation<ValidationSampleInput>> = validator.validate(input)
        assertThat(constraintViolations.size).isEqualTo(2)
    }

    /**
     * private val を使ってもOK
     */
    @Test
    fun inputIsInValidWithPrivateVal() {
        class ValidationSampleInput(@field:Max(10)
                                    private val numberBetweenOneAndTen: Int,
                                    @field:Pattern(regexp = "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$")
                                    private val ipAddress: String) {
        }

        val input = ValidationSampleInput(22, "fewhgoei2g234")
        val constraintViolations: Set<ConstraintViolation<ValidationSampleInput>> = validator.validate(input)
        assertThat(constraintViolations.size).isEqualTo(2)
    }


    /**
     * もともとnullableに定義しているため、コンパイルエラーが出る
     * kotlinを使っているからすぐにわかる
     */
//    @Test
//    fun manufacturerIsNull() {
//        val car = ValidationSampleCar(null, "DD-AB-123", 4)
//        val constraintViolations: Set<ConstraintViolation<ValidationSampleCar>> = validator.validate(car)
//        assertThat(constraintViolations.size).isEqualTo(1)
//        assertThat(constraintViolations.iterator().next().getMessage()).isEqualTo("must not be null")
//    }

    @Test
    fun licensePlateTooShort() {
        val car = ValidationSampleCar("Morris", "D", 4)
        val constraintViolations: Set<ConstraintViolation<ValidationSampleCar>> = validator.validate(car)
        assertThat(constraintViolations.size).isEqualTo(1)
        assertThat(
                constraintViolations.iterator().next().message
        ).isEqualTo("2 から 14 の間のサイズにしてください")
    }

    @Test
    fun seatCountTooLow() {
        val car = ValidationSampleCar("Morris", "DD-AB-123", 1)
        val constraintViolations: Set<ConstraintViolation<ValidationSampleCar>> = validator.validate(car)
        assertThat(constraintViolations.size).isEqualTo(1)
        assertThat(constraintViolations.iterator().next().message
        ).isEqualTo("2 以上の値にしてください")
    }

    @Test
    fun carIsValid() {
        val car = ValidationSampleCar("Morris", "DD-AB-123", 2)
        val constraintViolations: Set<ConstraintViolation<ValidationSampleCar>> = validator.validate(car)
        assertThat(constraintViolations.size).isEqualTo(0)
    }


    /**
     * その他細かいバリデーションもある
     */
    @Test
    fun variousValidationSampleIsInvalid() {
        val invalidObject = ValidationSampleObject(
                LocalDateTime.of(1998, 5, 12, 5, 2, 2),
                "4242424242424",
                "httgwrgrwaw")
        val constraintViolations: Set<ConstraintViolation<ValidationSampleObject>> = validator.validate(invalidObject)
        assertThat(constraintViolations.size).isEqualTo(3)
    }
    @Test
    fun variousValidationSampleIsValid() {
        val invalidObject = ValidationSampleObject(
                LocalDateTime.of(2099, 5, 12, 5, 2, 2),
                "4242424242424242",
                "https://www.google.com/")
        val constraintViolations: Set<ConstraintViolation<ValidationSampleObject>> = validator.validate(invalidObject)
        assert(constraintViolations.isEmpty())
    }
}