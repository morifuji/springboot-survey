package com.example.dddsurvey.domain.entity

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager

@DataJpaTest
class RepositoriesTest @Autowired constructor(
        val entityManager: TestEntityManager,
        val bookRepository: IBookRepository) {

    @Test
    fun `When findByTitle then return Article`() {
        val author = User("springjuergen", "Juergen", "Hoeller")
        entityManager.persist(author)
        val book1 = Book("hurry potter 1", author)
        entityManager.persist(book1)
        entityManager.flush()

        val found = bookRepository.findByTitle(book1.title)
        assertThat(found).isEqualTo(book1)

        val notFound = bookRepository.findByTitle("not found book")
        assertThat(notFound).isNull()
    }

    @Test
    fun `When findAllByOrderByTitle then ordering books`() {
        val author = User("springjuergen", "Juergen", "Hoeller")
        entityManager.persist(author)
        val books = arrayOf(
                Book("hurry potter 1", author),
                Book("_hurry potter 2", author),
                Book("hurry potter 3", author),
                Book("hurry potter 4", author),
                Book("hurry potter 2", author),
        )
        val orderedBooks = arrayOf(
                books[1],
                books[0],
                books[4],
                books[2],
                books[3],
        )
        books.forEach {
            entityManager.persist(it)
        }

        entityManager.flush()

        val founds = bookRepository.findAllByOrderByTitle()
        founds.forEachIndexed { index, book ->
            assertThat(book).isEqualTo(orderedBooks[index])
        }
    }

    @Test
    fun `When findAllByTitle then no book`() {
        val author = User("springjuergen", "Juergen", "Hoeller")
        entityManager.persist(author)
        val books = arrayOf(
                Book("hurry potter 1", author),
                Book("_hurry potter 2", author),
                Book("hurry potter 3", author),
                Book("hurry potter 4", author),
                Book("hurry potter 2", author),
        )
        books.forEach {
            entityManager.persist(it)
        }

        entityManager.flush()

        val found = bookRepository.findAllByTitle("hurry potter")
        assertThat(found.count()).isEqualTo(0)

        val found2 = bookRepository.findAllByTitle("hurry potter 2")
        assertThat(found2.count()).isEqualTo(1)
    }


    @Test
    fun `my custom method can work!!`() {
        val hogehoge = bookRepository.someCustomMethod()
        assertThat(hogehoge).isEqualTo("myBookRepository")
    }
}
