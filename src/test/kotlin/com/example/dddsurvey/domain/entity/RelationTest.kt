package com.example.dddsurvey.domain.entity

import com.example.dddsurvey.domain.entity.manyToMany.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.test.annotation.DirtiesContext
import java.time.LocalDateTime

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class RelationTest @Autowired constructor(
        val entityManager: TestEntityManager,
        val artistRepository: ArtistRepository,
        val musicRepository: MusicRepository) {

    @Test
    fun `save artist and id is updated`() {

        val artist = Artist("jypark", 35, null)

        entityManager.persist(artist)
        entityManager.flush()

        assertThat(artist.id).isEqualTo(1)
    }

    @Test
    fun `fetch artist`() {

        val artist = Artist("jypark", 35, null)

        entityManager.persist(artist)
        entityManager.flush()

        val fetchedArtist = artistRepository.findById(1)

        assertThat(fetchedArtist.get()).isEqualTo(artist)
    }

    @Test
    fun `fetch article when fething ranking`() {

        val artist = Artist("jypark", 35, null)
        entityManager.persist(artist)
        entityManager.flush()

        val ranking = Ranking(1, artist)
        entityManager.persist(ranking)
        entityManager.flush()

        val result = artistRepository.findById(1)
        val fetchedArtist = result.get()
        assertThat(fetchedArtist.ranking).isNull()

        fetchedArtist.ranking = ranking
        entityManager.persist(ranking)
        entityManager.flush()

        val refetchedArtist = artistRepository.findById(1).get()
        assertThat(refetchedArtist.ranking).isNotNull
    }

    @Test
    fun `fetch music when fetching artist`() {

        val artist = Artist("jypark", 35, null)
        entityManager.persist(artist)
        entityManager.flush()

        assertThat(artist.id).isEqualTo(1)

        val musicList = listOf<Music>(
                Music("Flavor of life", LocalDateTime.of(1998, 5, 12, 5, 2, 2), artist),
                Music("Traveling", LocalDateTime.of(2001, 2, 5, 5, 2, 2), artist),
                Music("僕はくま", LocalDateTime.of(2007, 1, 29, 17, 0), artist),
                Music("Goodbye Happiness", LocalDateTime.of(2006, 12, 2, 5, 2, 2), artist),
        )

        musicRepository.saveAll(musicList)

        val count = musicRepository.count()
        assertThat(count).isEqualTo(4)

//        val sort = Sort(ASC, "position")
//        val allMusic = musicRepository.findAll()
//
//        allMusic
    }

    @Test
    fun `sorting query`() {

        val artist = Artist("jypark", 35, null)
        entityManager.persist(artist)
        entityManager.flush()

        val musicList = listOf<Music>(
                Music("Flavor of life", LocalDateTime.of(1998, 5, 12, 5, 2, 2), artist),
                Music("Traveling", LocalDateTime.of(2001, 2, 5, 5, 2, 2), artist),
                Music("僕はくま", LocalDateTime.of(2007, 1, 29, 17, 0), artist),
                Music("Goodbye Happiness", LocalDateTime.of(2006, 12, 2, 5, 2, 2), artist),
        )
        musicRepository.saveAll(musicList)

        val result = musicRepository.findAll(PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC, "name"))).get()
        val fetchedMusicList = result.toArray()
        assertThat(fetchedMusicList.size).isEqualTo(2)
        assertThat(fetchedMusicList[1]).isEqualTo(musicList[3])
    }
}
