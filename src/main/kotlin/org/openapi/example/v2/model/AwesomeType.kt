package org.openapi.example.v2.model

import java.util.Objects
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.DecimalMax
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

/**
 * 
 * @param requiredString 
 * @param integerInt32 
 * @param integerInt64 
 * @param numberFloat 
 * @param numberDouble 
 * @param stringByte 
 * @param stringBinary 
 * @param stringDate 
 * @param stringDatetime 
 * @param stringPassword 
 */
data class AwesomeType(

    @get:NotNull 
    @JsonProperty("required_string") val requiredString: kotlin.String,
 @get:Max(4) 
    @JsonProperty("integer_int32") val integerInt32: kotlin.Int? = null,

    @JsonProperty("integer_int64") val integerInt64: kotlin.Long? = null,
@get:DecimalMin("4")
    @JsonProperty("number_float") val numberFloat: kotlin.Float? = null,

    @JsonProperty("number_double") val numberDouble: kotlin.Double? = null,
@get:Pattern(regexp="^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$") 
    @JsonProperty("string_byte") val stringByte: kotlin.ByteArray? = null,

    @JsonProperty("string_binary") val stringBinary: org.springframework.core.io.Resource? = null,

    @JsonProperty("string_date") val stringDate: java.time.LocalDate? = null,

    @JsonProperty("string_datetime") val stringDatetime: java.time.OffsetDateTime? = null,

    @JsonProperty("string_password") val stringPassword: kotlin.String? = null
) {

}

