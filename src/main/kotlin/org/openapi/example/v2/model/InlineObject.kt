package org.openapi.example.v2.model

import java.util.Objects
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.DecimalMax
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

/**
 * 
 * @param userName 
 * @param firstName 
 * @param lastName 
 */
data class InlineObject(

    @get:NotNull 
    @JsonProperty("userName") val userName: kotlin.String,

    @JsonProperty("firstName") val firstName: kotlin.String? = null,

    @JsonProperty("lastName") val lastName: kotlin.String? = null
) {

}

