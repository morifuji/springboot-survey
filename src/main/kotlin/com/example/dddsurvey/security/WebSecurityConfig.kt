package com.example.dddsurvey.security

import com.example.dddsurvey.application.UserDetailsService
import com.example.dddsurvey.security.SecurityConstants.SIGN_UP_URL
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource


@Configuration
class WebSecurityConfig(
        @Autowired private val bCryptPasswordEncoder: BCryptPasswordEncoder,
        @Autowired private val userDetailsService: UserDetailsService
) : WebSecurityConfigurerAdapter() {
    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
//        http
//                .authorizeRequests().antMatchers("/*").permitAll().anyRequest().authenticated().and()
//                .formLogin().loginPage("/login").permitAll().and()
//                .logout().permitAll()

        // デバッグ用
//        configureAllRequestPermitAll(http)
        configureH2Database(http)

        // JWT用
//        configureJwtWithoutCookie(http)
    }


    private fun configureJwtWithoutCookie(http: HttpSecurity) {
        http.cors().and().csrf().disable()
                // 認証が必要な画面
                .authorizeRequests().antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
                .anyRequest().authenticated()
                .and()
                // 認証フィルタ
                .addFilter(JWTAuthenticationFilter(authenticationManager()))
                // 認可フィルタ
                .addFilter(JWTAuthorizationFilter(authenticationManager()))
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    private fun configureAllRequestPermitAll(http: HttpSecurity) {
        http.authorizeRequests().antMatchers("/**").permitAll()
                .and().httpBasic().and().csrf().disable()
    }

    fun configureH2Database(http: HttpSecurity) {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/h2-console/**").permitAll();
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }

    @Throws(java.lang.Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder)
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource? {
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", CorsConfiguration().applyPermitDefaultValues())
        return source
    }
}