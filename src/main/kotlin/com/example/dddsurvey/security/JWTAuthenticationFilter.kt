package com.example.dddsurvey.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm.HMAC512
import com.example.dddsurvey.domain.entity.ApplicationUser
import com.example.dddsurvey.security.SecurityConstants.EXPIRATION_TIME
import com.example.dddsurvey.security.SecurityConstants.HEADER_STRING
import com.example.dddsurvey.security.SecurityConstants.LOGIN_URL
import com.example.dddsurvey.security.SecurityConstants.SECRET
import com.example.dddsurvey.security.SecurityConstants.TOKEN_PREFIX
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.io.IOException
import java.util.*
import javax.naming.AuthenticationException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JWTAuthenticationFilter(authManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter(authManager) {

    init {
        setFilterProcessesUrl(LOGIN_URL)
    }

    @Throws(AuthenticationException::class)
    override fun attemptAuthentication(req: HttpServletRequest,
                                       res: HttpServletResponse): Authentication {
        return try {
            val creds = ObjectMapper()
                    .readValue(req.inputStream, ApplicationUser::class.java)
            authenticationManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            creds.username,
                            creds.password,
                            ArrayList())
            )
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }

    @Throws(IOException::class, ServletException::class)
    override fun successfulAuthentication(req: HttpServletRequest?,
                                          res: HttpServletResponse,
                                          chain: FilterChain?,
                                          auth: Authentication) {
        val token: String = JWT.create()
                .withSubject((auth.principal as User).username)
                .withExpiresAt(Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(SECRET))
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token)
    }
}