package com.example.dddsurvey.infrastructure

import com.example.dddsurvey.domain.entity.Book
import com.example.dddsurvey.domain.entity.IBookRepository
import com.example.dddsurvey.domain.entity.User
import com.example.dddsurvey.domain.entity.IUserRepository
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BookConfiguration {
    @Bean
    fun databaseInitializer(userRepository: IUserRepository,
                            bookRepository: IBookRepository) = ApplicationRunner {

        val author = User("springjuergen", "Juergen", "Hoeller")
        userRepository.save(author)
        val books = listOf(
                Book("hurry potter 1", author),
                Book("_hurry potter 2", author),
                Book("hurry potter 3", author),
                Book("hurry potter 4", author),
                Book("hurry potter 2", author),
        )

        bookRepository.saveAll(books)
    }
}