package com.example.dddsurvey.presentation.controller

import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.lang.Exception

interface RawErrorResolver {
    fun getRawErrorString(ex: Exception): String?
}

@Component
@Profile("default")
class RawErrorResolverDefault() : RawErrorResolver {
    override fun getRawErrorString(ex: Exception): String? {
        return ex.toString()
    }
}

@Component
@Profile("production")
class RawResponseResolverProduction() : RawErrorResolver {
    override fun getRawErrorString(ex: Exception): String? {
        return null
    }
}
