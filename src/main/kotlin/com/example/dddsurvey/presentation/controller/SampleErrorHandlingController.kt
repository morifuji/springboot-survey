package com.example.dddsurvey.presentation.controller

import com.example.dddsurvey.application.BookService
import com.example.dddsurvey.domain.entity.ApplicationUser
import com.example.dddsurvey.domain.entity.Book
import com.example.dddsurvey.presentation.dto.ValidationSampleCar

import org.openapi.example.v2.api.AwesomeApi
import org.openapi.example.v2.model.AwesomeType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.config.web.servlet.SecurityMarker
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@Validated
@RequestMapping("/errorhandling")
class SampleErrorHandlingController(@Autowired private val bookService: BookService) {

    @RequestMapping(
            value = ["/valid"],
            method = [RequestMethod.POST])
    fun valid(@RequestBody @Valid car: ValidationSampleCar): ResponseEntity<ValidationSampleCar> {
        return ResponseEntity.ok(car)
    }

    @RequestMapping(
            value = ["/invalid"],
            method = [RequestMethod.POST])
    fun invalid(@RequestBody @Valid car: ValidationSampleCar): ResponseEntity<ValidationSampleCar> {
        return ResponseEntity.ok(car)
    }
}