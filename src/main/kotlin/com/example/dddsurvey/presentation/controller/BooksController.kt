package com.example.dddsurvey.presentation.controller

import com.example.dddsurvey.application.BookService
import com.example.dddsurvey.domain.entity.Book
import com.example.dddsurvey.domain.entity.IBookRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam


@Controller
public class BooksController(@Autowired private val bookService: BookService) {
    @GetMapping("/book")
    fun top(model: Model, @RequestParam query: String?): String {
//        model["title"] = "Blog"
        model["books"] = if (query == null) listOf() else bookService.searchBooks(query).map(Book::toString)
        return "top"
    }
}


