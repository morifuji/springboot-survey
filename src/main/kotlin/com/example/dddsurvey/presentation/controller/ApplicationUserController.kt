package com.example.dddsurvey.presentation.controller

import com.example.dddsurvey.domain.entity.ApplicationUser
import com.example.dddsurvey.domain.entity.ApplicationUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/users")
class ApplicationUserController(@Autowired private val applicationUserRepository: ApplicationUserRepository) {

    private val bCryptPasswordEncoder = BCryptPasswordEncoder()

    @PostMapping("/sign-up")
    fun signUp(@RequestBody user: ApplicationUser): ApplicationUser {
        user.password = bCryptPasswordEncoder.encode(user.password)
        applicationUserRepository.save(user)
        return user
    }

    @PostMapping("/users")
    fun view(): List<ApplicationUser> {
        return applicationUserRepository.findAll()
    }


    @DeleteMapping("/users/:id")
    fun delete(@PathVariable id: Long) {
        return applicationUserRepository.deleteById(id)
    }
}