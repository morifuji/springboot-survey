package com.example.dddsurvey.presentation.controller

import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

open class NotFoundException(id: Long) : RuntimeException("Could not find data. data is $id")
open class BadRequestException(id: Long, fieldErrors: Map<String, List<String>>)
    : RuntimeException(
        "fuck request!. id=$id, message=" + fieldErrors.asIterable().fold("") { a, v ->
            a + v.value.joinToString("\n") + "\n\n"
        })

data class ErrorItem(val objectName: String?, val message: String, val rejectedValue: Any?, val args: List<String>)

data class ErrorResponse(val status: HttpStatus, val message: String, val errors: List<ErrorItem>, val rawError: Any? = null)

/**
 * 例外処理を記述している。
 *
 * - https://github.com/JahnelGroup/spring-boot-samples/blob/master/spring-boot-api-error/src/main/kotlin/com/example/api/errors/WebApiExceptionHandler.kt
 * - https://github.com/khandelwal-arpit/springboot-starterkit/blob/master/src/main/java/com/starterkit/springboot/brs/exception/CustomizedResponseEntityExceptionHandler.java
 * などなど
 */
@ControllerAdvice
class ControllerAdvicer(@Autowired val rawErrorResolver: RawErrorResolver) : ResponseEntityExceptionHandler() {

    @ResponseBody
    @ExceptionHandler(NotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun notFoundHandler(ex: NotFoundException): String? {
        return "404" + ex.message
    }

    @ResponseBody
    @ExceptionHandler(BadRequestException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun badRequestHandler(ex: BadRequestException): String? {
        return "400" + ex.message
    }

    /**
     * うまくシリアライズできなかった場合
     * ex. notnullにnullがバインドされた場合
     */
    override fun handleHttpMessageNotReadable(ex: HttpMessageNotReadableException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        val cause = ex.rootCause
        val body = if (cause is MissingKotlinParameterException) {
            val fieldName = cause.path[0].fieldName
            ErrorResponse(status, "${fieldName}が不正な値です", listOf(ErrorItem(fieldName, "${fieldName}が不正な値です", null, listOf())), rawErrorResolver.getRawErrorString(ex))
        } else {
            ErrorResponse(status, "不正な値が含まれています", listOf(ErrorItem(null, "不正な値が含まれています", null, listOf())), rawErrorResolver.getRawErrorString(ex))
        }

        return ResponseEntity.status(status).body(body)
    }

//    @ResponseBody
//    @ExceptionHandler(HttpRequestMethodNotSupportedException::class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    fun requestMethodNotFoundHandler(ex: BadRequestException): String? {
//        return "400" + "存在しません"
//    }

    @ResponseBody
    @ExceptionHandler(RuntimeException::class)
    fun allExceptionHandler(ex: RuntimeException): String? {
        println(ex)
        return "400" + ex.message
    }

    /**
     * validationを通過しなかった場合
     */
    override fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        val errorItems = ex.allErrors.map { objectError ->
            if (objectError is FieldError) {
                return@map ErrorItem(objectError.field, objectError.defaultMessage
                        ?: "", objectError.rejectedValue, objectError.arguments.map { it.toString() }.toList())
            }
            ErrorItem("", objectError.defaultMessage ?: "", "", objectError.arguments.map { it.toString() }.toList())
        }
        val body = ErrorResponse(status, ex.message ?: "", errorItems, rawErrorResolver.getRawErrorString(ex))
        return ResponseEntity.status(status).body(body)
    }

    @ResponseBody
    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun allExceptionHandler(ex: Exception): String? {
        return "404" + ex.message
    }
}