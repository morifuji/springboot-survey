package com.example.dddsurvey.presentation.controller

import com.example.dddsurvey.application.BookService
import com.example.dddsurvey.application.transaction.PersonService
import com.example.dddsurvey.domain.entity.Book
import com.example.dddsurvey.domain.entity.IBookRepository
import com.example.dddsurvey.domain.entity.transaction.Person
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/transaction")
public class TransactionController(@Autowired private val service: PersonService) {
    /**
     * 排他ロックがかかってそう（H2が原因である可能性もある）
     *
     */
    @GetMapping("")
    fun readTransaction(): List<Person> {
        return service.readTransaction()
    }

    /**
     * 2回叩くと20秒待つ
     * writeTransactionWithUpdateと叩くと10秒
     */
    @GetMapping("/add")
    fun writeTransaction(): Person {
        return service.writeTransaction()
    }

    /**
     * 2回叩くと20秒待つ
     * writeTransactionと叩くと10秒
     */
    @GetMapping("/update")
    fun writeTransactionWithUpdate(): Person {
        return service.writeTransactionWithUpdate()
    }
}
