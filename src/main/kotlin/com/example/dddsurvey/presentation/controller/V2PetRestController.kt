package com.example.dddsurvey.presentation.controller

import com.example.dddsurvey.application.BookService
import com.example.dddsurvey.domain.entity.Book

import org.openapi.example.v2.api.AwesomeApi
import org.openapi.example.v2.model.AwesomeType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.config.web.servlet.SecurityMarker
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

import org.springframework.web.bind.annotation.RestController

@RestController
@Validated
class V2PetRestController(@Autowired private val bookService: BookService) : AwesomeApi {


    @SecurityMarker
    override fun securityOperationId1(): ResponseEntity<AwesomeType> {
        return super.securityOperationId1()
    }

    override fun securityOperationId2(): ResponseEntity<AwesomeType> {
        return super.securityOperationId2()
    }

    override fun securityOperationId3(): ResponseEntity<AwesomeType> {
        return super.securityOperationId3()
    }

    override fun securityOperationId4(): ResponseEntity<AwesomeType> {
        return super.securityOperationId4()
    }

//    override fun createPets(): ResponseEntity<Unit> {
//        return super.createPets()
//    }
//
//    override fun listPets(limit: Int?): ResponseEntity<List<Pet>> {
//        val hoge = listOf(
//                Pet(
//                        id = 1,
//                        name = "わんちゃん1",
//                        tag = "でかい,ご飯よく食べる"
//                ),
//                Pet(
//                        id = 2,
//                        name = "かめの大五郎",
//                        tag = "遅い,よく寝てる"
//                )
//        )
//        return ResponseEntity.ok(hoge)
//    }
//
//    @RequestMapping(
//            value = ["/hogehoge"],
//            method = [RequestMethod.GET])
//    fun hoge(): String {
//        return "######"
//    }
//
//
//    @GetMapping("/api/hoge/books")
//    fun all(): Iterable<Book> {
//        return bookService.findAllBooks()
//    }
//
//    override fun showPetById(petId: String): ResponseEntity<Pet> {
//        return super.showPetById(petId)
//    }
}