package com.example.dddsurvey.presentation.controller

import com.example.dddsurvey.application.BookService
import com.example.dddsurvey.domain.entity.Book
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.*
import javax.validation.ConstraintViolationException
import javax.validation.Valid
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Pattern


class BookNotFoundException(id: Long) : BadRequestException(id, mapOf(
        "id" to listOf("id is valid!"),
        "dummy" to listOf("dummy is valid!!", "is exist?"),
))


class Input {
    @Min(1)
    @Max(10)
    val numberBetweenOneAndTen = 0

    @Pattern(regexp = "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$")
    val ipAddress: String? = null

    fun getNumber(): String {
        return numberBetweenOneAndTen.toString()
    }
}


@RestController
@Validated
class BooksRestController(@Autowired val bookService: BookService) {
    @GetMapping("/api/books")
    fun all(): Iterable<Book> {
        return bookService.findAllBooks()
    }

    @PostMapping("/api/test/{id}")
    fun test(@PathVariable("id") @Min(5) id: Int): String {
        return id.toString()
    }

    @PostMapping("/api/test2/{id}")
    fun test2(@PathVariable("id") @Min(5) id: Int, @RequestBody input: Input): String {
        return id.toString()
    }

    @PostMapping("/api/test3/{id}")
    fun test3(@PathVariable("id") @Min(5) id: Int, @Valid @RequestBody input: Input): String {
        return id.toString()
    }

    @PostMapping("/api/test4/{id}")
    fun test4(@Valid @RequestBody input: Input, @PathVariable("id") @Min(5) id: Int): String {
        return id.toString()
    }

    @ExceptionHandler(ConstraintViolationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleConstraintViolationException(e: ConstraintViolationException): ResponseEntity<String>? {
        return ResponseEntity("ConstraintViolationException!!!!: " + e.message, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleMethodArgumentNotValidException(e: ConstraintViolationException): ResponseEntity<String>? {
        return ResponseEntity("MethodArgumentNotValidException!!!!!: " + e.message, HttpStatus.BAD_REQUEST)
    }

    @GetMapping("/api/books/{id}")
    fun one(@PathVariable id: Long): Book? {
        return bookService.findById(id)
                .orElseThrow { BookNotFoundException(id) }
    }
}

//@RestController
//class PetsApiController : PetsApi {
//    fun listPets(@Valid limit: Int?): ResponseEntity<List<Pet>> {
//        println("Here list pet")
//        return ResponseEntity<List<Pet>>(HttpStatus.OK)
//    }
//}

//@RestController
//class PetsApiController : PetsApi {
//    fun listPets(@Valid limit: Int?): ResponseEntity<List<Pet>> {
//        println("Here list pet")
//        return ResponseEntity<List<Pet>>(HttpStatus.OK)
//    }
//}