package com.example.dddsurvey.presentation.dto

import org.hibernate.validator.constraints.CreditCardNumber
import org.hibernate.validator.constraints.URL
import java.time.LocalDateTime
import javax.validation.constraints.Future
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ValidationSampleCar(@field:NotNull
                          private val manufacturer: String,
                          @field:NotNull
                          @field:Size(min = 2, max = 14)
                          private val licensePlate: String,
                          @field:Min(2)
                          private val seatCount: Int
) {
}

class ValidationSampleObject(
        @field:Future
        private val future: LocalDateTime,

        @field:CreditCardNumber
        private val creditCardNumber: String,

        @field:URL
        private val url: String
) {
}
