package com.example.dddsurvey.application.transaction

import com.example.dddsurvey.domain.entity.Book
import com.example.dddsurvey.domain.entity.IBookRepository
import com.example.dddsurvey.domain.entity.transaction.Person
import com.example.dddsurvey.domain.entity.transaction.PersonRepository
import org.springframework.data.jpa.repository.Lock
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime
import javax.persistence.LockModeType

@Service
class PersonService(private var personRepository: PersonRepository) {

    @Transactional(readOnly = true)
    fun readTransaction(): List<Person> {
        val personList = personRepository.findAll()

        // 10秒停止
        println("start wait..." + Thread.currentThread().id)
        Thread.sleep(10 * 1000)
        println("stop wait..." + Thread.currentThread().id)
        return personList
    }

    @Transactional(readOnly = false)
    fun writeTransaction(): Person {
        val newPerson = Person("morifuji", LocalDateTime.of(1994, 1, 1, 1, 1))

        personRepository.save(newPerson)

        // 10秒停止
        println("start wait..." + Thread.currentThread().id)
        Thread.sleep(10 * 1000)
        println("stop wait..." + Thread.currentThread().id)

        return newPerson
    }

    @Transactional(readOnly = false)
    fun writeTransactionWithUpdate(): Person {
        val person = personRepository.findById(1)

        val p = person.get()
        p.name = p.name + "_v_"

        personRepository.save(p)

        // 10秒停止
        println("start wait..." + Thread.currentThread().id)
        Thread.sleep(10 * 1000)
        println("stop wait..." + Thread.currentThread().id)

        return p
    }
}