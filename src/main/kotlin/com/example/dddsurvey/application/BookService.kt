package com.example.dddsurvey.application

import com.example.dddsurvey.domain.entity.Book
import com.example.dddsurvey.domain.entity.IBookRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.util.*

@Service
class BookService(private var bookRepository: IBookRepository) {

    fun searchBooks(query: String): Iterable<Book> {
        return bookRepository.findAllByOrderByTitle()
    }

    fun findAllBooks(): Iterable<Book> {
        return bookRepository.findAll()
    }

    fun findById(id: Long): Optional<Book> {
        return bookRepository.findById(id)
    }
}