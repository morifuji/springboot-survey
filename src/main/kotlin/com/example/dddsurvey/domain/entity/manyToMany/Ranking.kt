package com.example.dddsurvey.domain.entity.manyToMany

import javax.persistence.*


@Entity
@Table(name = "ranking")
class Ranking(
        private var rank: Int,
        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "artist_id")
        private val artist: Artist
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private val id: Long = 0
}