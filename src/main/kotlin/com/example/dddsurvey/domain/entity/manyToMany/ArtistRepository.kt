package com.example.dddsurvey.domain.entity.manyToMany

import com.example.dddsurvey.domain.entity.ApplicationUser
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ArtistRepository : JpaRepository<Artist, Long?> {
}