package com.example.dddsurvey.domain.entity

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface IBookRepository : CrudRepository<Book, Long>, IBookRepositoryCustom {
    fun findByTitle(title: String): Book?
    fun findAllByTitle(title: String): Iterable<Book>
    fun findAllByOrderByTitle(): Iterable<Book>
}

/**
 * カスタムを作るときは、
 * - {XXXX}Customインタフェース
 * - {XXXX}Implクラス
 * が必要
 */
interface IBookRepositoryCustom {
    fun someCustomMethod(): String
}
open class IBookRepositoryImpl : IBookRepositoryCustom {
    override fun someCustomMethod(): String {
        return "myBookRepository"
    }
}


@Repository
interface IUserRepository : CrudRepository<User, Long> {
}