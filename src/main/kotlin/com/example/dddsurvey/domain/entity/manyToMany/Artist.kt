package com.example.dddsurvey.domain.entity.manyToMany

import javax.persistence.*

@Entity
@Table(name = "artist")
public class Artist(
        private var name: String,
        private var age: Int,
        @OneToOne(mappedBy = "artist", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        var ranking: Ranking?,

        @OneToMany(mappedBy = "artist", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        var musics: MutableList<Music> = mutableListOf()
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0
}
