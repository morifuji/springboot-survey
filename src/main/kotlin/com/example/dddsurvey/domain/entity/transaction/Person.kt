package com.example.dddsurvey.domain.entity.transaction

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime
import javax.persistence.*

@Entity // JPA エンティティとして扱う
@Table(name = "person") // DBテーブル情報
class Person(
        @Column(name = "name")
        @JsonProperty("name")
        var name: String,

        @Column(name = "created_at")
        @JsonProperty("created_at")
        private val createdAt: LocalDateTime
) {
    @Id // JPA にこの変数をオブジェクトのIDだと認識させる
    @GeneratedValue(strategy = GenerationType.IDENTITY) // ID自動生成
    @Column(name = "id")
    @JsonProperty("id") // マッピングする JSON キー (名前)
    private val id: Long? = null
}