package com.example.dddsurvey.domain.entity.manyToMany

import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "music")
public class Music(
        var name: String,
        @Column(name = "publish_date")
        var publishDate: LocalDateTime,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "company_id")
        var artist: Artist?
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0


    override fun equals(other: Any?): Boolean {
        val casted = (other as Music)
        return casted.name === name
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}
