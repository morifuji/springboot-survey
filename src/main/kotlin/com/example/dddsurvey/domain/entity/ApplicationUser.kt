package com.example.dddsurvey.domain.entity

import javax.persistence.*


@Entity
class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0
    var username: String? = null
    var password: String? = null
}