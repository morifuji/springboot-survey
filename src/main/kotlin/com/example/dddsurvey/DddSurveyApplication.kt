package com.example.dddsurvey

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DddSurveyApplication

fun main(args: Array<String>) {
    runApplication<DddSurveyApplication>(*args)
}
