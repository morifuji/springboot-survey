# PetsApi

All URIs are relative to *http://petstore.swagger.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPets**](PetsApi.md#createPets) | **POST** /pets | Create a pet
[**listPets**](PetsApi.md#listPets) | **GET** /pets | List all pets
[**showPetById**](PetsApi.md#showPetById) | **GET** /pets/{petId} | Info for a specific pet


<a name="createPets"></a>
# **createPets**
> createPets()

Create a pet

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.example.dddsurvey.dto.*

val apiInstance = PetsApi()
try {
    apiInstance.createPets()
} catch (e: ClientException) {
    println("4xx response calling PetsApi#createPets")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PetsApi#createPets")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="listPets"></a>
# **listPets**
> kotlin.Array&lt;Pet&gt; listPets(limit)

List all pets

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.example.dddsurvey.dto.*

val apiInstance = PetsApi()
val limit : kotlin.Int = 56 // kotlin.Int | How many items to return at one time (max 100)
try {
    val result : kotlin.Array<Pet> = apiInstance.listPets(limit)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PetsApi#listPets")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PetsApi#listPets")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **kotlin.Int**| How many items to return at one time (max 100) | [optional]

### Return type

[**kotlin.Array&lt;Pet&gt;**](Pet.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="showPetById"></a>
# **showPetById**
> Pet showPetById(petId)

Info for a specific pet

### Example
```kotlin
// Import classes:
//import org.openapitools.client.infrastructure.*
//import com.example.dddsurvey.dto.*

val apiInstance = PetsApi()
val petId : kotlin.String = petId_example // kotlin.String | The id of the pet to retrieve
try {
    val result : Pet = apiInstance.showPetById(petId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling PetsApi#showPetById")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling PetsApi#showPetById")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **petId** | **kotlin.String**| The id of the pet to retrieve |

### Return type

[**Pet**](Pet.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

