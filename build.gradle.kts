import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    dependencies {
        classpath("org.openapitools:openapi-generator-gradle-plugin:4.2.1")
    }
}

plugins {
    id("org.springframework.boot") version "2.4.0-SNAPSHOT"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.4.0"
    kotlin("plugin.spring") version "1.4.0"
    kotlin("plugin.jpa") version "1.4.0"
    kotlin("plugin.allopen") version "1.4.0"

    // for openapi generator
    // https://tech-blog.optim.co.jp/entry/2020/04/13/100000
    id("org.openapi.generator") version "4.3.1"
}

//group = "com.example"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

apply {
    plugin("java")
    plugin("kotlin")
}

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
    maven { url = uri("https://repo.spring.io/snapshot") }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-mustache")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    /**
     * see https://ja.stackoverflow.com/a/67046/29189
     */
    implementation("org.springframework.boot:spring-boot-starter-validation")

    runtimeOnly("com.h2database:h2")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    // for openapi generator
    implementation(group = "org.openapitools", name = "jackson-databind-nullable", version = "0.1.0")

    // for spring boot security
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.security:spring-security-test")

    // for thymthelfs
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")

    // for JWT
    implementation("com.auth0:java-jwt:3.11.0")
}

//
openApiGenerate {
    generatorName.set("kotlin-spring")
    inputSpec.set("$rootDir/petstore_v2.yml")
    outputDir.set("$rootDir")
    apiPackage.set("org.openapi.example.v2.api")
    invokerPackage.set("org.openapi.example.v2.invoker")
    modelPackage.set("org.openapi.example.v2.model")
    /**
     * https://openapi-generator.tech/docs/generators/kotlin-spring/
     */
    configFile.set("$rootDir/openapi/config.json")
}

tasks.compileKotlin {
    dependsOn(":openApiGenerate")
}

sourceSets.main {
    java.srcDir("${openApiGenerate.outputDir}/src/main/kotlin")
    resources.srcDir("${openApiGenerate.outputDir}/src/main/kotlin")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {

        /**
         * see https://github.com/OpenAPITools/openapi-generator/issues/5476
         */
//        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

// JPA 遅延フェッチ
allOpen {
    annotation("javax.persistence.Entity")
    annotation("javax.persistence.Embeddable")
    annotation("javax.persistence.MappedSuperclass")
}


// for hotreload
tasks.bootRun {
    sourceResources(sourceSets["main"])
}